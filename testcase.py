import unittest

from pom.base import BaseUtil
import testcases.testcase2 as t2
import testcases.test_login as tl
from drivers.HTMLTestRunner import  HTMLTestRunner

def create_allTest_testing():
    # 测试套件
    suite = unittest.TestSuite()
    # 测试用例加载器
    loader = unittest.TestLoader()
    # 搜索所有的测试用例
    allTests = loader.discover(start_dir='testcases',pattern='test**.py')
    print(allTests)
    # 测试套件添加测试用例
    suite.addTests(allTests)
    return suite


def create_smoke_testing():
    suite = unittest.TestSuite()
    loader = unittest.TestLoader()
    tests1 = loader.loadTestsFromModule(t2)
    tests2 = loader.loadTestsFromModule(tl)
    print(tests1,tests2)
    suite.addTests([tests1,tests2])
    return suite


def runner_default():
    # 创建测试套件
    with open('report.txt','w') as f:
        runner = unittest.TextTestRunner(stream=f, verbosity=2)
        # 获取测试套件
        suite = create_allTest_testing()
        smokeTests = create_smoke_testing()
        runner.run(smokeTests)

def runner_html_report():
    # 创建runner
    smoketest = create_smoke_testing()
    # 报告文件放到统一目录reports,每次运行的时候文件名使用日期格式；
    with open('report.html',mode='wb') as f:
        runner = HTMLTestRunner(stream=f,title='cndoe.js test',
            description='主功能测试')
        runner.run(smoketest)
if __name__ == '__main__':

    # runner_html_report()
    runner_default()
    BaseUtil.close_window()
