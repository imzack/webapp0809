from selenium import webdriver
import os

from selenium.webdriver.common.by import By

chromedriver = os.path.join(os.path.dirname(__file__),'../drivers/chromedriver.exe')
print(chromedriver)

class Client:

    def __init__(self):
        self.driver = webdriver.Chrome(executable_path=chromedriver)
        self.driver.implicitly_wait(10)
        self.driver.get('http://49.233.108.117:3000/')

    def action(self,element,**kwargs):
        ele = self.driver.find_element(*element)
        ac = kwargs.get('action')
        if ac == "sendkeys":
            ele.send_keys(kwargs.get('value'))
        elif ac == 'click':
            ele.click()

if __name__ == '__main__':
    import csv

    client = Client()
    file = os.path.join(os.path.dirname(__file__),'../data/data.csv')
    with open(file, encoding='utf8') as f:
        reader = csv.DictReader(f)
        for line in reader:
            print(line)
            client.action((By.XPATH, line['element_xpath']),action=line['action'],value=line['value'])


    #
    # client.action((By.LINK_TEXT,'登录'),action='click')
    # client.action((By.ID,'name'),action='sendkeys',value='test10')
    # client.action((By.ID, 'pass'), action='sendkeys', value='123456')
    # client.action((By.CSS_SELECTOR, '[type="submit"]'), action='click')