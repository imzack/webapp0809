"""
测试登录场景
包含
1.正常场景
2.异常场景
"""

try:
    import yaml
except ImportError:  # pragma: no cover
    have_yaml_support = False
else:
    have_yaml_support = True

print(have_yaml_support)
import  unittest
import os,sys

is_win32 = sys.platform == 'win32'

needwindows = unittest.skipUnless(is_win32,'需要操作系统为Windows')



class Test1(unittest.TestCase):


    def test01(self):
        assert True

    @needwindows
    def test02(self):
        assert True

    @unittest.skipIf(sys.platform =='linux', 'Linux')
    def test03(self):
        assert True

    @unittest.skip('这次不执行')
    def test04(self):
        assert False

if __name__ == '__main__':
    unittest.main(verbosity=2)