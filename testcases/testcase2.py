import unittest

from bussiness.main_model import MainModel

# 定义test类<必须继承 unittest.TestCase>
from pom.base import BaseUtil


class TestLogin(unittest.TestCase):

    def setUp(self) -> None:
        """每一个testcase 执行之前的操作"""
        pass


    def tearDown(self) -> None:
        """ 每一个testcase 执行完成之后的操作"""
        BaseUtil.save_screenshot()
        BaseUtil.driver.delete_all_cookies()
        BaseUtil.driver.get('http://49.233.108.117:3000/')

    @classmethod
    def setUpClass(cls) -> None:
        """整个类运行执行前操作"""
        BaseUtil.max_window()




    # 测试方法  test开头
    def test_login(self):
        main = MainModel()
        user = main.go_to_login_page()
        user.user_login('test10', '123456')

        login_name = main.user_name_text
        self.assertEqual(login_name,'test10')

    # 测试注册
    def test_register(self):
        main = MainModel()
        user = main.go_to_register_page()
        user.user_register('test1','123456','123456','test1@gmail.com')

        regiser_fail_text = user.get_register_result()
        self.assertEqual(regiser_fail_text,'用户名或邮箱已被使用。')
if __name__ == '__main__':
    unittest.main()