### 代码结构

- bussiness 业务层 封装了主要业务模块
- common 封装一些常用的操作或者一些工具类
- data 存放一些测试数据
- drivers 存放浏览器驱动
- Pom 页面对象模型
- testcases 存放测试用例 ddt
